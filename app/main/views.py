from datetime import datetime

from flask import flash, render_template, session, redirect, url_for, current_app, request, jsonify
from datatables import ColumnDT, DataTables
from .. import db
from ..models import User, Task
from . import main
from .forms import NameForm

@main.route('/', methods=['GET', 'POST'])
def index():
    return render_template('index.html')

@main.route('/tasks/<username>', methods=['GET', 'POST'])
def tasks(username):
    return render_template('tasks.html', name=username)

@main.route('/data')
def data():
    """Return server side data."""
    # defining columns
    columns = []
    columns.append(ColumnDT('id'))
    columns.append(ColumnDT('priority'))
    columns.append(ColumnDT('text'))
    columns.append(ColumnDT('created_date'))

    # defining the initial query depending on your purpose
    query = db.session.query(User).join(Task)

    # instantiating a DataTable for the query and table needed
    rowTable = DataTables(request.args, User, query, columns)

    # returns what is needed by DataTable
    return jsonify(rowTable.output_result())
